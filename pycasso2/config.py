'''
Created on 24/06/2015

@author: andre
'''
import os
import sys

if sys.version[0] == '2':
    from ConfigParser import SafeConfigParser as ConfigParser
else:
    from configparser import ConfigParser

import pycasso2

__all__ = ['default_config_path', 'get_config', 'save_config']

default_config_path = '%s/data/pycasso.cfg' % os.path.dirname(os.path.abspath(pycasso2.__file__))


def get_config(configfile='pycasso.cfg'):
    if not os.path.exists(configfile):
        raise FileNotFoundError("Cannot find configfile %s." % configfile)
    config = ConfigParser(os.environ)
    config.read(configfile)
    return config


def save_config(cfg, configfile):
    with open(configfile, 'w') as cfp:
        cfg.write(cfp)
